function EEG_analysis_master(patient)
%Panna H 19/09


root = 'F:\Human\raw_EEG_data\';
savedir = ['F:\Human\preproc_EEG_data\' patient '\'];
if ~isdir(savedir)
    mkdir(savedir);
end
filesource = [root patient '\'];
files = dir([filesource '*.vhdr']);

for i = 1:length(files)
cfg = [];
cfg.dataset = [filesource files(i).name];
data_eeg = ft_preprocessing(cfg) %Make eeg files matlab compatible

save([savedir cfg.dataset(28:(end-5)) '_preproc.mat'] ,'data_eeg');
end
% %Probe plotting
% chansel  = 1;
% plot(data_eeg.time{1}, data_eeg.trial{1,1}(chansel, :))
% xlabel('time (s)')
% ylabel('channel amplitude (uV)')
% legend(data_eeg.label(chansel))
% close(gcf);

preproc_EEG_ica(patient)

preproc_EEG_rmvcmp