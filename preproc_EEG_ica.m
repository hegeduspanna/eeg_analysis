function preproc_EEG_ica(patient)

info_s3.home             = 'C:\Users\hegedus.panna\Documents\MATLAB\EEG_analysis\';
info_s3.filesource          = ['F:\Human\preproc_EEG_data\' patient '\'];
info_s3.export           = 'F:\Human\filtered_EEG_data\';

addpath ('C:\Users\hegedus.panna\Documents\fieldtrip-20190830');   ft_defaults;

%--------------------------------------------------------------------------
% go to folder where data is
%--------------------------------------------------------------------------

files_preproc = dir ([info.filesource '*.mat']);


%--------------------------------------------------------------------------
% load
%--------------------------------------------------------------------------
% for k = 1:length(files_preproc)
for k = 1:1
    load ([info.filesource ,'/' files_preproc(k).name])
    info.layout         = 'C:\Users\hegedus.panna\Documents\fieldtrip-20190830\template\layout\acticap-64ch-standard2.mat';
    info.home           = 'C:\Users\hegedus.panna\Documents\MATLAB\EEG_analysis\';
    info.ftFolder       = 'C:\Users\hegedus.panna\Documents\fieldtrip-20190830'; % fieldtrip folder
    info.s3             = info_s3;
    info.fsample = data_eeg.fsample;
    info.preproc = {'hp = 1' 'lp = 90' 'bs = 47 53' 'dft = yes' 'lp44, blackman'};
    info.filename   = char(files_preproc(k).name);
    info.Subject    = patient;
    info.Condition  = info.filename(8:23);
    %
    %--------------------------------------------------------------------------
    % perform the independent component analysis
    %--------------------------------------------------------------------------
    
    cfg        = [];
    cfg.method = 'runica';
    comp = ft_componentanalysis(cfg, data_eeg);
    
    
    %--------------------------------------------------------------------------
    % clear what is not needed and save workspace
    %--------------------------------------------------------------------------
    clearvars ans cfg
    saveto = [info.export, '/' info.Subject, '_', info.Condition, '_ica'];
    save (saveto);
    
end

