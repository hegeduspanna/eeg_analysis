%--------------------------------------------------------------------------
% preparation
%--------------------------------------------------------------------------

info.import = 'F:\Human\filtered_EEG_data\';
info.export = 'F:\Human\filtered_EEG_data\rmvcomp';


%--------------------------------------------------------------------------
% go to folder where data is
%--------------------------------------------------------------------------

files_ica = dir ([info.import '/*.mat'])


%--------------------------------------------------------------------------
% loop/load files
%--------------------------------------------------------------------------
for k = 1:length(files_ica)   
    
    load ([info.import ,'/' files_ica(k).name])
        clear info_s3 saveto

    info.task = 'SSRT';
    disp (info.Subject)
    %--------------------------------------------------------------------------
    %  plot comps topo + timeseries
    %--------------------------------------------------------------------------
    cfg                   =[];
    cfg.blocksize         = 10;
    cfg.ylim              = [-5 5];
    cfg.zlim              = 'maxabs'
    cfg.channel           = 1:4;
    cfg.continuous        = 'yes';
    cfg.colormap          = 'jet';
    cfg.layout = info.layout;
    cfg.compscale          = 'local';
    cfg.viewmode          = 'component';
    ft_databrowser(cfg, comp);
    colormap jet;
    
   % --------------------------------------------------------------------------
   %  plot topography only
   % --------------------------------------------------------------------------
  
for ind = 1:20:100       %numel(comp.label)
    cfg = [];
    cfg.layout = info.layout;
    cfg.component = [ind:ind+19]
    figure('units','normalized','outerposition',[0.1 0.1 0.3 0.8]);
    ft_topoplotIC(cfg, comp, data_eeg);
end
    
    
    
    %--------------------------------------------------------------------------
    %  plot components with ICABROWSER (freq,topo)
    %--------------------------------------------------------------------------
    
    cfg                = [] ;
    cfg.path           = 'F:\Human\filtered_EEG_data\rmvcomp'
    cfg.prefix         = 'icabrowser';
    cfg.layout = info.layout;
    cfg.colormap       = 'jet';
%     bad_comps=ft_icabrowser_task6(cfg, comp);
    bad_comps=ft_icabrowser(cfg, comp);   
    
   
    
    %--------------------------------------------------------------------------
    % remove the bad components and backproject the data
    %--------------------------------------------------------------------------
    cfg = [];
    
    %  enter components you want to remove from the data into bad_comp:
%     cfg.component = 
    info.rmv_components = cfg.component';
    data_ICA_rmv = ft_rejectcomponent(cfg, comp);
    
    
    %--------------------------------------------------------------------------
    %  reconstruct missing channels
    %--------------------------------------------------------------------------
    % I. method: neighbours
    
    cfg             = [];
    cfg.layout      = 'GSN-HydroCel-256.sfp'
    load ('/home/manuel/project/scripts/PDGS/neighbours_HydroCel-256.mat')
    cfg.neighbours  = neighbours;
    cfg.missingchannel = {neighbours((~ismember({neighbours(:).label}, data_ICA_rmv.label))).label};
    info.rmv_channel = cfg.missingchannel';
    data_EEG_clean = ft_channelrepair(cfg,data_ICA_rmv);
    
    % II. method: spline (used if there are not sufficient neighbours to
    % reconstruct a channel
%     cfg.missingchannel  = {neighbours((~ismember({neighbours(:).label}, data_EEG_clean.label))).label};
%     cfg.method          = 'spline';
%     data_EEG_clean      = ft_channelrepair(cfg,data_EEG_clean);
%     
%     info.bad_comps=bad_comps;
%     info.s4 = info_s4;
    %--------------------------------------------------------------------------
    % clear what is not needed and save workspace
    %--------------------------------------------------------------------------
    
    saveto = [info.s4.export, '/' info.Subject, '_', info.Condition, '_rmvcomp'];
    save ([saveto], 'data_EEG_clean', 'info');
    
end

